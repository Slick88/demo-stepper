import React from 'react';
import PageWrapper from "../../containers/PageWrapper";
import styled from "styled-components";
import {useDispatch} from "react-redux";
import { reset as resetPage2} from '../Page1/page1Slice';
import { reset as resetPage1 } from '../Page2/page2Slice';

interface Props { history: string[]; }

const StyledH1 = styled.h1`
  font-size: 80px;
  color: blue;
`

const StyledButton = styled.button`
  display: block;
  margin: 50px auto 0 auto;  
  font-size: 30px;
  padding: 0 100px;
  color: aliceblue;
  background-color: #211e1e;
`

const handleOnClick = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, history: string[], dispatch: any) => {
    dispatch(resetPage1())
    dispatch(resetPage2())
    history.push('/');
}

const Page3 = (props: Props) => {
    const dispatch = useDispatch()

    return (
        <div>
            <StyledH1>Success!</StyledH1>
            <StyledButton onClick={(e) => handleOnClick(e, props.history, dispatch)}>
                Home
            </StyledButton>
        </div>
    );
}

export default PageWrapper(Page3);