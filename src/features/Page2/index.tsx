import React from 'react';
import PageWrapper from "../../containers/PageWrapper";
import InputField from "../../components/InputField";
import styled from "styled-components";
import {useDispatch, useSelector} from "react-redux";
import {selectPartValues, updatePartValues, statusUpdate} from './page2Slice';
import {selectNumberOfParts} from "../Page1/page1Slice";

interface Props { history: string[]; }

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
`
const StyledButtonBar = styled.div`
  display: flex;
  justify-content: flex-end;
  
  button {
  background-color: blue;
  color: aliceblue;
    :disabled{
      background-color: hsl(229, 100%, 82%);
    }
    font-size: 30px;
    padding: 0 50px;
    color: aliceblue;
    margin: 50px 30px 0 0;
    
    :last-of-type{
    margin-right: 0px
    }
  }
  
  
  button.secondary {
    background-color: #211e1e;
  }
`

const validationCheck = (sumArray: number[]) => {
    const reducer = (accumulator: number, currentValue: number) => accumulator + currentValue;
    const sum = sumArray.reduce(reducer, 0)
    return sum === 100
}

const Page2 = (props: Props) => {
    const dispatch = useDispatch()
    const numberOfParts = useSelector(selectNumberOfParts)
    const partValuesData = useSelector(selectPartValues)

    const handleChange = (index: number, value: number) => {
        let temp: number[] = [...partValuesData];
        temp[index] = value
        dispatch(updatePartValues(temp))
        if(validationCheck(temp)) {
            dispatch(statusUpdate(true))
        } else {
            dispatch(statusUpdate(false))
        }
    }

    const renderInputs = () => {
        const inputs = []
        for (let i = 0; i < numberOfParts; ++i) {
            inputs.push(<InputField label={`Part ${i+1} %`} value={partValuesData[i]} onChange={(e) => handleChange(i,Number(e.target.value))}/>);
        }
        return inputs
    }

    return (
        <>
            <StyledContainer>
                {renderInputs()}
                <StyledButtonBar>
                    <button className={'secondary'} onClick={() => props.history.push('/page1')}>
                        Previous
                    </button>
                    <button  onClick={() => props.history.push('/page3')} disabled={!validationCheck(partValuesData)}>
                        Next
                    </button>
                </StyledButtonBar>
            </StyledContainer>

        </>
    );
}

export default PageWrapper(Page2);