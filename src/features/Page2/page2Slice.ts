import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface Page2State {
  completed: boolean;
  partValues: number[];
}

const initialState: Page2State = {
  completed: false,
  partValues: [],
};

export const page2Slice = createSlice({
  name: 'page2',
  initialState,
  reducers: {
    updatePartValues: (state, action: PayloadAction<number[]>) => {
      state.partValues = action.payload;
    },
    statusUpdate: (state, action: PayloadAction<boolean>) => {
      state.completed = action.payload;
    },
    reset: (state) => {
      state.partValues = initialState.partValues;
      state.completed = initialState.completed;
    }
  },
});

export const { updatePartValues, statusUpdate, reset } = page2Slice.actions;

export const selectStatus = (state: RootState) => state.page2.completed;

export const selectPartValues = (state: RootState) => state.page2.partValues;

export default page2Slice.reducer;
