import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface Page1State {
  completed: boolean;
  numberOfParts: number;
}

const initialState: Page1State = {
  completed: false,
  numberOfParts: 0,
};

export const page1Slice = createSlice({
  name: 'page1',
  initialState,
  reducers: {
    updateNumberOfParts: (state, action: PayloadAction<number>) => {
      state.numberOfParts = action.payload;
    },
    statusUpdate: (state, action: PayloadAction<boolean>) => {
      state.completed = action.payload;
    },
    reset: (state) => {
      state.numberOfParts = initialState.numberOfParts;
      state.completed = initialState.completed;
    }
  },
});

export const { updateNumberOfParts, statusUpdate, reset } = page1Slice.actions;

export const selectStatus = (state: RootState) => state.page1.completed;

export const selectNumberOfParts = (state: RootState) => state.page1.numberOfParts;

export default page1Slice.reducer;
