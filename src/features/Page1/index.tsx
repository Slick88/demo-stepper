import React from 'react';
import PageWrapper from "../../containers/PageWrapper";
import styled from "styled-components";
import InputField from "../../components/InputField";
import {useDispatch, useSelector} from "react-redux";
import { updateNumberOfParts, statusUpdate, selectNumberOfParts } from './page1Slice';

const StyledButtonBar = styled.div`
  display: flex;
  justify-content: flex-end;
  
  button {
  background-color: blue;
  color: aliceblue;
    :disabled{
      background-color: hsl(229, 100%, 82%);
    }
    font-size: 30px;
    padding: 0 50px;
    color: aliceblue;
    margin: 50px 30px 0 0;
    
    :last-of-type{
    margin-right: 0px
    }
  }
  
  
  button.secondary {
    background-color: #211e1e;
  }
`


interface Props { history: string[]; }

const Page1 = (props: Props) => {
    const dispatch = useDispatch();

    const number = useSelector(selectNumberOfParts);

    const onChange = (event: any) => {
        event.stopPropagation();
        const input = event.target.value
        dispatch(updateNumberOfParts(input));
        if(Number(input) > 1) {
            dispatch(statusUpdate(true));
        } else {
            dispatch(statusUpdate(false));
        }
    }

    return (
        <div>
            <InputField label={'Number Of Parts:'} type="number" value={number} valid={Number(number) > 0} onChange={onChange}/>

            <StyledButtonBar>
                <button className={'secondary'} onClick={() => props.history.push('/')}>
                    Previous
                </button>
                <button  onClick={() => props.history.push('/page2')} disabled={!(Number(number) > 1)}>
                    Next
                </button>
            </StyledButtonBar>
        </div>
    )
}

export default PageWrapper(Page1);