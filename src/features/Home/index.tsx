import React from 'react';
import styled from "styled-components";

const StyledHome = styled.div`
  display: flex;
  justify-content: center;
  margin: 100px;
`

interface Props { history: string[]; }

const StyledButton = styled.button`
  display: block;
  margin: 50px auto 0 auto;  
  font-size: 30px;
  padding: 0 100px;
  color: #f0f8ff;
  background-color: dodgerblue;
  border-color: dodgerblue;
`

const Home = (props: Props ) => {
    return (
        <StyledHome>
            <StyledButton onClick={() => props.history.push('/page1')}>New Request</StyledButton>
        </StyledHome>
    );
}

export default Home;