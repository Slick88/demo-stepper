import React from 'react';
import styled from "styled-components";

interface Props {
    label?: string;
    type?: string;
    value: string|number;
    valid?: boolean;
    onChange(arg0: any): void;
}

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 10px;
  label {
  min-width: 60px;
  }
`

const StyledInput = styled.input`
 margin-left: 5px;
 border-width: 2px;
 border-color: ${(props: { valid: boolean }) => props.valid ? `green` : `red`}
`

const InputField = (props: Props) => {
    return (
        <StyledContainer>
            {props.label && <label >{props.label}</label>}
            <StyledInput
                type={props.type}
                value={props.value}
                valid={props.valid || true}
                onChange={props.onChange}
            />
        </StyledContainer>
    );
}

export default InputField;