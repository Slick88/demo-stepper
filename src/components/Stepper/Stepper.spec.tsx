import React from 'react';
import {mount, ReactWrapper} from 'enzyme';
import Stepper, {Props} from './index';

let mainWrapper: ReactWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>;

const render = (props: Props) => mount(
    <Stepper {...props}/>
);


describe('Main page' , () => {
    beforeEach(() => {
        mainWrapper = render({
            stepList: [{
                num: 1,
                completed: true
            }, {
                num: 2,
                completed: false
            }]
        });
    })
    it('should render correctly', function () {
        expect(mainWrapper).toMatchSnapshot()
        expect(mainWrapper.exists('.step-wrapper')).toEqual(true)
        expect(mainWrapper.find('.step-wrapper')).toHaveLength(2)
        mainWrapper.setProps({ stepList: [{
                num: 1,
                completed: true
            }, {
                num: 2,
                completed: false
            }, {
                num: 3,
                completed: false
            }]
        });
        expect(mainWrapper.find('.step-wrapper')).toHaveLength(3)
    });
})