import React from 'react';
import styled from 'styled-components'

export interface Step {
    num: number;
    completed: boolean;
}

export interface Props {
    stepList: Step[]
}

const StyledWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    
    .step-wrapper {
        width: 23%;
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        position: relative;
        
        
        
        .divider-line {
            height: 1px;
            width: 100%;
            background-color: black;
            position: absolute;
            top: 25px;
            left: 50%;
            z-index: -1;
        }
    }

    
`;

const StyledNumber = styled.div` 
    border-radius: 50%;
    border: 1px solid grey;
    background-color: ${(props: { completed: boolean }) => props.completed ? `hsl(243, 100%, 50%)` : `hsl(229, 100%, 82%)`};
    width: 50px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    color: aliceblue;
    font-size: x-large;
    z-index: 1;
`

const renderSteps = (stepList: any) => {
    return stepList.map((step:any, index: number) => {
        return (
            <div className="step-wrapper">
                <StyledNumber completed={step.completed}>
                    {step.num}
                </StyledNumber>
                {index !== stepList.length-1 && <div className="divider-line" />}
            </div>
        )
    })
}

const Stepper = (props: Props) => {
    return (
        <StyledWrapper>
            {renderSteps(props.stepList)}
        </StyledWrapper>
    );
}

export default Stepper;