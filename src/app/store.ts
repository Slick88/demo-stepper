import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import page1Reducer from '../features/Page1/page1Slice';
import page2Reducer from '../features/Page2/page2Slice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    page1: page1Reducer,
    page2: page2Reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
