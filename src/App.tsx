import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from "./features/Home";
import Page1 from "./features/Page1";
import Page2 from "./features/Page2";
import Page3 from "./features/Page3";
import './App.css';
import {store} from "./app/store";

function App() {
  return (
      <BrowserRouter>
        <Switch>
            <Route exact path={'/page1'} store={store} component={Page1} />
            <Route exact path={'/page2'} store={store} component={Page2} />
            <Route exact path={'/page3'} store={store} component={Page3} />
            <Route store={store} component={Home} />
        </Switch>
      </BrowserRouter>
  );
}

export default App;
