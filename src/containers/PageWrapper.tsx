import React  from 'react';
import { useSelector } from 'react-redux';
import Stepper from "../components/Stepper";
import styled from "styled-components";
import {selectStatus} from "../features/Page1/page1Slice";
import {selectStatus as selectp2Status} from "../features/Page2/page2Slice";

export interface Props {
    location: {
        pathname: string;
    };
}

const StyledContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 100px;
`

const StyledStepperContainer = styled.div`
  border: 1px solid #ffa07a;
  border-radius: 3px;
  margin: 40px auto;
  padding: 30px;
  width: 60%;
  box-shadow: 0 0 5px coral;
`

const PageWrapper = (WrapperComponents: any) => {
    return (props: Props) => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const page1Status = useSelector(selectStatus)
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const page2Status = useSelector(selectp2Status)

        return (
            <>
                {/*dummy data*/}
                <StyledStepperContainer>
                    <Stepper stepList={[{num: 1, completed: page1Status}, {num: 2, completed: page2Status}, {num: 3, completed: props.location.pathname.slice(-1) === '3'}]}/>
                </StyledStepperContainer>
                <StyledContainer>
                    <WrapperComponents {...props} />
                </StyledContainer>
             </>
        );
    };
};

export default PageWrapper;
